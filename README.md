I am pleased to announce the version 0.0.1 of gneural_network.

Even if the author has worked hardly and accurately on this project, surely, some bugs are still present.

The implementation of this advanced program is brought to you by the efforts of Jean Michel Sellier.

The basic algorithms implemented in this software are described in the documentation of gneural_network.

See the file INSTALL for building and installation instructions.

Please send all bug reports by electronic mail to:
    jeanmichel.sellier@gmail.com

gneural_network is free software.  See the file COPYING for copying conditions.

## 简介

> 本程序为一个基础1-4-1三层神经网络模型，代码由`Jean Michel Sellier`实现，代码清晰易懂，可帮助理解神经网络算法.

## 添加

* 基础代码的核心注释
